package no.ntnu.idagh;

import java.util.HashMap;
import java.util.Objects;
/**
 *  A class that administers a department.
 *  @version 26.03.2021
 *  @author idagh
 */

public class Department {
    private String departmentName;
    private HashMap<String, Employee> employees = new HashMap<>();
    private HashMap<String, Patient> patients = new HashMap<>();

    /**
     * @param departmentName
     */
    public Department(String departmentName) {
        this.departmentName = departmentName;
    }

    /**
     * @return departmentname
     */
    public String getDepartmentName() {
        return departmentName;
    }

    /**
     * @param departmentName
     */
    public void setDepartmentName(String departmentName) {
        this.departmentName = departmentName;
    }

    /**
     * @return employee-list
     */
    public HashMap<String, Employee> getEmployees() {
        return employees;
    }


    /**
     * Method addPatient adds employee if the employee
     * is not already registered, and throws an
     * IllegalArgumentException if it is
     * @param e employee that is going to be added
     */
    public void addEmployee(Employee e) {
        try {
            if (!employees.containsKey(e.getSocialSecurityNumber())) { //checks if the employee already exists, via SSN
                employees.put(e.getSocialSecurityNumber(), e);
            } else {
                throw new IllegalArgumentException("The employee is already registered");
            }
        } catch (IllegalArgumentException ex) {
            ex.printStackTrace();
        }
    }

    /**
     * @return patient-list
     */
    public HashMap<String, Patient> getPatients() {
        return patients;
    }

    /**
     * Method addPatient adds patient if the patient
     * is not already registered, and throws an
     * IllegalArgumentException if it is
     * @param p patient that is going to be added
     */
    public void addPatient(Patient p) {
        try {
            if (!patients.containsKey(p.getSocialSecurityNumber())) { //checks if the patient already exists, via SSN
                patients.put(p.getSocialSecurityNumber(), p);
            } else {
                throw new IllegalArgumentException("The patient is already registered");
            }
        } catch (IllegalArgumentException ex) {
            ex.printStackTrace();
        }
    }

    /**
     * Method to remove an employee or patient that
     * throws a RemoveException if not successful
     * (changed the method to return a boolean, to
     * make it easier to test)
     * @param p person that is supposed to be removed
     * @return true if removal was successful, false if not
     * @throws RemoveException
     */
    public boolean remove(Person p) throws RemoveException {
        try {
            if (getEmployees().containsKey(p.getSocialSecurityNumber())) {
                getEmployees().remove(p.getSocialSecurityNumber());
                return true;
            } else if (getPatients().containsKey(p.getSocialSecurityNumber())) {
                getPatients().remove(p.getSocialSecurityNumber());
                return true;
            }
            throw new RemoveException("Person could not be found");
        }
        catch (RemoveException ex) {
            ex.printStackTrace();
        }
        return false;
    }

    /**
     * @param o the reference object with which to compare
     * @return true if department names are equal, and false if not
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Department that = (Department) o;
        return Objects.equals(departmentName, that.departmentName);
    }

    /**
     * @return a hashcode value for this object
     */
    @Override
    public int hashCode() {
        return Objects.hash(departmentName);
    }

    /**
     * @return department-object as string
     */
    @Override
    public String toString() {
        return "Department: " + departmentName + "\nEmployees:\n" + employees.toString() +
                "\nPatients:\n" + patients.toString();
    }
}