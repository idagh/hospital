package no.ntnu.idagh;
/**
 *  A class that represents a Nurse.
 *  @version 26.03.2021
 *  @author idagh
 */
public class Nurse extends Employee{
    public Nurse(String firstName, String lastName, String socialSecurityNumber) {
        super(firstName, lastName, socialSecurityNumber);
    }

    /**
     * @return nurse-object in string
     */
    @Override
    public String toString() {
        return super.toString();
    }
}
