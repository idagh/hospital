package no.ntnu.idagh;

import java.util.HashMap;

/**
 *  A class that administers a hospital.
 *  @version 26.03.2021
 *  @author idagh
 */
public class Hospital {
    private final String HOSPITALNAME;
    private HashMap<String, Department> departments = new HashMap<>();

    public Hospital(String hospitalname) {
        HOSPITALNAME = hospitalname;
    }

    /**
     * @return hospitalname
     */
    public String getHOSPITALNAME() {
        return HOSPITALNAME;
    }

    /**
     * @return departments
     */
    public HashMap<String, Department> getDepartments() {
        return departments;
    }

    /**
     * Method addDepartment adds department in departments
     * (department-register) if the department is not
     * already registrered. If it is, an
     * IllegalArgumentException is thrown.
     * @param d
     */
    public void addDepartment(Department d) {
        try {
            if (!departments.containsValue(d)) {
                departments.put(d.getDepartmentName(), d);
            } else {
                throw new IllegalArgumentException("The department is already registered");
            }
        } catch (IllegalArgumentException ex) {
            ex.printStackTrace();
        }

    }
}
