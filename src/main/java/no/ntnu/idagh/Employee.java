package no.ntnu.idagh;

/**
 *  A class that represents an employee.
 *  @version 26.03.2021
 *  @author idagh
 */
public class Employee extends Person{
    /**
     * Constructor
     * @param firstName
     * @param lastName
     * @param socialSecurityNumber
     */
    public Employee(String firstName, String lastName, String socialSecurityNumber) {
        super(firstName, lastName, socialSecurityNumber);
    }

    /**
     * @return employee-object in a string
     */
    @Override
    public String toString() {
        return super.toString();
    }
}
