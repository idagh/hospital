package no.ntnu.idagh;

/**
 *  A class that represents a patient.
 *  @version 26.03.2021
 *  @author idagh
 */
public class Patient extends Person implements Diagnosable{
    private String diagnosis = "";

    /**
     * Constructor
     * @param firstName
     * @param lastName
     * @param socialSecurityNumber
     */
    protected Patient(String firstName, String lastName, String socialSecurityNumber) {
        super(firstName, lastName, socialSecurityNumber);
    }

    /**
     * @return diagnosis
     */
    protected String getDiagnosis() {
        return diagnosis;
    }

    /**
     * @param diagnosis
     */
    public void setDiagnosis(String diagnosis) {
        this.diagnosis = diagnosis;
    }

    /**
     * @return patient-object in string
     */
    @Override
    public String toString() {
        return super.toString() + "\nDiagnosis: " + diagnosis;
    }
}
