package no.ntnu.idagh;

/**
 *  A class that represents a Doctor.
 *  @version 26.03.2021
 *  @author idagh
 */
public abstract class Doctor extends Employee{
    /**
     * Constructor
     * @param firstName
     * @param lastName
     * @param socialSecurityNumber
     */
    protected Doctor(String firstName, String lastName, String socialSecurityNumber) {
        super(firstName, lastName, socialSecurityNumber);
    }

    /**
     * @param patient
     * @param diagnosis
     */
    public abstract void setDiagnosis(Patient patient, String diagnosis);
}
