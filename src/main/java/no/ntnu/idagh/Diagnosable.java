package no.ntnu.idagh;

public interface Diagnosable {
    public void setDiagnosis(String diagnosis);
}
