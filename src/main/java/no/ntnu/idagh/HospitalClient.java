package no.ntnu.idagh;

public class HospitalClient {
    public static void main(String[] args) throws RemoveException {
        Hospital hospital = new Hospital("Sykehuset");
        HospitalTestData.fillRegisterWithTestData(hospital);

        Department emergency = hospital.getDepartments().get("Akutten");
        Employee oddEven = emergency.getEmployees().get("1");
        //removes Odd Even Primtall from employees in emergency-department
        emergency.remove(oddEven);

        Department childrenPolyclinic = hospital.getDepartments().get("Barn poliklinikk");
        Patient hansOmvar = childrenPolyclinic.getPatients().get("16");
        //tries to remove a patient that does not exist in the department
        try {
            emergency.remove(hansOmvar);
        } catch (RemoveException ex) {
            ex.printStackTrace();
        }

        //prints out the hospital's information, to check that everything has been executed properly
        System.out.println(hospital.getDepartments());
    }
}