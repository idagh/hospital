package no.ntnu.idagh;

public class RemoveException extends Exception {
    private static final long serialVersionUID = 1L;

    public RemoveException(String message) {
        super(message);
    }
}
