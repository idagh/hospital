package no.ntnu.idagh;

public class GeneralPractitioner extends Doctor{
    /**
     * Constructor
     * @param firstName
     * @param lastName
     * @param socialSecurityNumber
     */
    public GeneralPractitioner(String firstName, String lastName, String socialSecurityNumber) {
        super(firstName, lastName, socialSecurityNumber);
    }

    /**
     * @param patient
     * @param diagnosis
     */
    public void setDiagnosis(Patient patient, String diagnosis) {
        patient.setDiagnosis(diagnosis);
    }
}
