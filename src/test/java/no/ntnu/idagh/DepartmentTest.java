package no.ntnu.idagh;

import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.util.HashMap;

import static org.junit.jupiter.api.Assertions.*;

class DepartmentTest {
    Department department = new Department("department");
    Employee e1 = new Employee("Odd Even", "Primtallet", "123");
    Patient p1 = new Patient("Inga", "Lykke", "125");

    @Nested
    class remove {
        @Test
        void testRemoveSuccessEmployee() {
            department.addEmployee(e1);

            try {
                assertTrue(department.remove(e1));
            } catch (RemoveException ex) {
                ex.printStackTrace();
            }
        }

        @Test
        void testRemoveSuccessPatient() {
            department.addPatient(p1);

            try {
                assertTrue(department.remove(p1));
            } catch (RemoveException ex) {
                ex.printStackTrace();
            }
        }

        @Test
        void testRemoveUnsuccessfulEmployee() {
           try {
                assertFalse(department.remove(e1));
            } catch (RemoveException ex) {
                ex.printStackTrace();
            }
        }

        @Test
        void testRemoveUnsuccessfulPatient() {
            try {
                assertFalse(department.remove(p1));
            } catch (RemoveException ex) {
                ex.printStackTrace();
            }
        }
    }
}